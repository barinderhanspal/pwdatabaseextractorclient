package com.programmableweb.recommender.usercf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class UserApiIDGenerator {
	
	private HashMap<String, Long> userIdMap;
	private HashMap<String, Long> apiIdMap;
	
	private HashMap<Long, String> userNameMap;
	private HashMap<Long, String> apiNameMap;
	
	
	public void generateUserApiIDDataFromFile(String inputFilePath, String outputFilePath ) throws IOException, ClassNotFoundException, SQLException{
			
		File inputFile = new File(inputFilePath);
		FileReader inputFileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(inputFileReader);
		
		File outputFile = new File(outputFilePath);
		FileWriter outputFileWrite = new FileWriter(outputFile);
		BufferedWriter bufferedWriter = new BufferedWriter(outputFileWrite);
	
		String line = null, userName = null, apiName = null;
		long apiIdCount = 0l, userIdCount = 0l ;
		
		userIdMap = new HashMap<String, Long>();
		apiIdMap = new HashMap<String, Long>();
		
		userNameMap = new HashMap<Long, String>();
		apiNameMap = new HashMap<Long, String>();
		
		while((line = bufferedReader.readLine()) != null){
		//	System.out.println(line);
			String[] userApi = line.split(",");
			userName = userApi[0];
			apiName = userApi[1];
		//	System.out.println("user = " + userId);
		//	System.out.println("api = " + api);
			
			if(!userIdMap.containsKey(userName)){
				userIdMap.put(userName, userIdCount);
				userNameMap.put(userIdCount, userName);
				++userIdCount;
			}
			
			if(!apiIdMap.containsKey(apiName)){
				apiIdMap.put(apiName, apiIdCount);
				apiNameMap.put(apiIdCount, apiName);
				++apiIdCount;
			}
			
			bufferedWriter.write(userIdMap.get(userName) + "," + apiIdMap.get(apiName));
			bufferedWriter.newLine();
		}
		
		bufferedWriter.close();
		outputFileWrite.close();
		bufferedReader.close();
		inputFileReader.close();
		
		//return new HashMap<Long, Set<Long>>();
	}
	
	public Long getUserId(String userName){
		long userId = -1l;
		if(this.userIdMap.containsKey(userName))
			userId = this.userIdMap.get(userName);
		
		return userId;
	}
	
	public Long getApiId(String apiName){
		long apiId = -1;
		if(this.apiIdMap.containsKey(apiName))
			apiId = this.apiIdMap.get(apiName);
		return apiId;
	}
	
	public String getUserName(Long userId){
		return this.userNameMap.get(userId);
	}
	
	public String getApiName(Long apiName){
		return this.apiNameMap.get(apiName);
	}
	
	protected HashMap<String, Long> getUserIdMap(){
		return this.userIdMap;
	}
	
	protected HashMap<String, Long> getApiIdMap(){
		return this.apiIdMap;
	}
	
	protected HashMap<Long, String> getUserNameMap(){
		return this.userNameMap;
	}
	
	protected HashMap<Long, String> getApiNameMap(){
		return this.apiNameMap;
	}	
	public void printIdMap(HashMap<String, Long> map){
		List<Entry<String, Long>> mapToList = new ArrayList<Entry<String, Long>>(map.entrySet());
		
		for(Entry<String, Long> entry : mapToList){
			System.out.println("Name = " + entry.getKey() + ", ID = " + entry.getValue());
		}
	}
	
	public void printNameMap(HashMap<Long, String> map){
		List<Entry<Long, String>> mapToList = new ArrayList<Entry<Long, String>>(map.entrySet());
		
		for(Entry<Long, String> entry : mapToList){
			System.out.println("id = " + entry.getKey() + ", value = " + entry.getValue());
		}
	}
	
	/*public static void main(String args[]) throws ClassNotFoundException, IOException, SQLException{
		
		long startTime = System.currentTimeMillis();
		UserApiIDGenerator userApiGenerator = new UserApiIDGenerator();
		userApiGenerator.generateUserApiIDDataFromFile();
		long endTime = System.currentTimeMillis();
		
		System.out.println("Printing user id map : ");
		//userApiGenerator.printIdMap(userApiGenerator.getUserIdMap());
		
		System.out.println("\nPrinting api id map : ");
		//userApiGenerator.printIdMap(userApiGenerator.getApiIdMap());
		
		System.out.println("**********************************");
		
		System.out.println("Printing user name map : ");
		userApiGenerator.printNameMap(userApiGenerator.getUserNameMap());
		
		System.out.println("\nPrinting api name map : ");
		userApiGenerator.printNameMap(userApiGenerator.getApiNameMap());
		System.out.println("Time taken = " + ((endTime - startTime)/1000) + " seconds");
	}*/
}