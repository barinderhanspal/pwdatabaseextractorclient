package com.programmableweb.recommender.usercf;

import java.io.File;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericBooleanPrefUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.TanimotoCoefficientSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;

public class PWUserRecommenderEvaluator {

	public static void main(String[] args) throws Exception {

		DataModel model = new FileDataModel(new File("CSVFiles/UserApiIDData.csv"));

		RecommenderBuilder builder = new RecommenderBuilder() {
			@Override
			public Recommender buildRecommender(DataModel model) throws TasteException {
				//UserSimilarity similarity = new TanimotoCoefficientSimilarity(model);
				UserSimilarity similarity = new LogLikelihoodSimilarity(model);
				//UserNeighborhood neighborhood =new ThresholdUserNeighborhood(0.9, similarity, model);
				
				UserNeighborhood neighborhood = new NearestNUserNeighborhood(10, similarity, model);
				return new GenericBooleanPrefUserBasedRecommender(model, neighborhood, similarity);
			}
		};
		//RandomUtils.useTestSeed();
		RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();
		IRStatistics stats = evaluator.evaluate(builder, 
				null, 
				model, 
				null, 
				10, 
				GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 
				1);

		// on average, about P % of recommendations are good
		System.out.println("PRECISION: On Avarege, about " + stats.getPrecision()*100.0 + "% of recommendations are good" );

		// %R of good recommenations are amont those recommended
		System.out.println("RECALL: " + stats.getRecall()*100.0 + " % of good recommenations are among those recommended");
		//System.out.println("F-SCORE: " + stats.getF1Measure()*100.0 + " %");

	}

}
