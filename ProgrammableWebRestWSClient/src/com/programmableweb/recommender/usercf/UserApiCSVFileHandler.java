package com.programmableweb.recommender.usercf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.programmableweb.restwebservice.client.UserApiExtractor;

public class UserApiCSVFileHandler {
	
		public void writeUserApiDataToFile() throws IOException, ClassNotFoundException, SQLException{

		String outputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "LatestUserApiData.csv";

		File outputFile = new File(outputFilePath);
		//System.out.println("Writing USER-API data to file : " + outputFilePath);	
		FileWriter outputFileWrite = new FileWriter(outputFile);
		BufferedWriter bufferedWriter = new BufferedWriter(outputFileWrite);

		UserApiExtractor userApiExtractor = new UserApiExtractor();
		List<Entry<String, Set<String>>> userApiList = new ArrayList<Entry<String, Set<String>>>(userApiExtractor.getAllApisUsedByUsers().entrySet());

		for(Entry<String, Set<String>> userApiEntry : userApiList){
			//System.out.println("User_id = " + userApiEntry.getKey() + "\n" + "Api = " + userApiEntry.getValue() );

			List<String> apisUsedByCurrentUserList = new ArrayList<String>(userApiEntry.getValue());

			for(String api : apisUsedByCurrentUserList){
				bufferedWriter.write(userApiEntry.getKey() + "," + api);
				bufferedWriter.newLine();
			}
		}
		bufferedWriter.close();
		outputFileWrite.close();
		//System.out.println("Data write complete...");
	}

	public void writeUserApisDataUsedBeforeTimestampToFile(Timestamp apiUsageTimeStamp) throws IOException, ClassNotFoundException, SQLException{

		String outputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "HistoricUserApiData.csv";

		File outputFile = new File(outputFilePath);
		//System.out.println("Writing USER-API data to file : " + outputFilePath);	
		FileWriter outputFileWrite = new FileWriter(outputFile);
		BufferedWriter bufferedWriter = new BufferedWriter(outputFileWrite);

		UserApiExtractor userApiExtractor = new UserApiExtractor();
		List<Entry<String, Set<String>>> userApiList = new ArrayList<Entry<String, Set<String>>>(userApiExtractor.getAllApisUsedByUsers(apiUsageTimeStamp).entrySet());

		for(Entry<String, Set<String>> userApiEntry : userApiList){
			//System.out.println("User_id = " + userApiEntry.getKey() + "\n" + "Api = " + userApiEntry.getValue() );

			List<String> apisUsedByCurrentUserList = new ArrayList<String>(userApiEntry.getValue());

			for(String api : apisUsedByCurrentUserList){
				bufferedWriter.write(userApiEntry.getKey() + "," + api);
				bufferedWriter.newLine();
			}
		}
		bufferedWriter.close();
		outputFileWrite.close();
		//System.out.println("Data write complete...");
	}

	private HashMap<String, Set<String>> extractUserApiData(String inputFilePath) throws IOException{

		File inputFile = new File(inputFilePath);
		FileReader inputFileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(inputFileReader);

		HashMap<String, Set<String>> userApisMap = new HashMap<String, Set<String>>();

		String userName = null, apiName = null, line = null;

		while((line = bufferedReader.readLine()) != null){
			//	System.out.println(line);
			String[] userApi = line.split(",");
			userName = userApi[0];
			apiName = userApi[1];
			//	System.out.println("user = " + userId);
			//	System.out.println("api = " + api);
			if(userApisMap.containsKey(userName)){
				userApisMap.get(userName).add(apiName);
			} else {
				userApisMap.put(userName, new HashSet<String>());
				userApisMap.get(userName).add(apiName);
			}
		}

		bufferedReader.close();
		inputFileReader.close();

		return userApisMap;
	}

	public HashMap<String, Set<String>> extractLatestUserApiData() throws IOException{
		
		String inputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "LatestUserApiData.csv";
		return extractUserApiData(inputFilePath);
	}
	
	public HashMap<String, Set<String>> extractHistoricUserApiData() throws IOException{
		
		String inputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "HistoricUserApiData.csv";
		return extractUserApiData(inputFilePath);
	}


	/*public static void main(String args[]) throws ClassNotFoundException, IOException, SQLException{

		long startTime = System.currentTimeMillis();
		UserApiCSVFileHandler userApiFileHandler = new UserApiCSVFileHandler();
		userApiFileHandler.writeUserApiDataToFile();
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken = " + ((endTime - startTime)/1000) + " seconds");
	}*/
}