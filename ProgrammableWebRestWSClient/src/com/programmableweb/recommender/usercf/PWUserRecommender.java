package com.programmableweb.recommender.usercf;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.DataModelBuilder;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.common.FastByIDMap;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.model.BooleanPreference;
import org.apache.mahout.cf.taste.impl.model.BooleanUserPreferenceArray;
import org.apache.mahout.cf.taste.impl.model.GenericBooleanPrefDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericBooleanPrefUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.TanimotoCoefficientSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.PreferenceArray;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class PWUserRecommender {

	/**
	 * @return the userNameHistoricalMap
	 */
	public HashMap<Long, String> getUserNameHistoricalMap() {
		return userNameHistoricalMap;
	}

	/**
	 * @return the apiNameHistoricalMap
	 */
	public HashMap<Long, String> getApiNameHistoricalMap() {
		return apiNameHistoricalMap;
	}

	/**
	 * @return the userNameLatestMap
	 */
	public HashMap<Long, String> getUserNameLatestMap() {
		return userNameLatestMap;
	}

	/**
	 * @return the apiNameLatestMap
	 */
	public HashMap<Long, String> getApiNameLatestMap() {
		return apiNameLatestMap;
	}


	private static HashMap<Long, String> userNameHistoricalMap;
	private static HashMap<Long, String> apiNameHistoricalMap;
	private static HashMap<Long, String> userNameLatestMap;
	private static HashMap<Long, String> apiNameLatestMap;
	
	private static Recommender historicalRecommender = null;
	private static UserApiIDGenerator userApiIDGeneratorHistorical = null;
	
	private static Recommender latestDataRecommender  = null;
	private static UserApiIDGenerator userApiIDGeneratorLatestData = null;

	public void initializeHistoricalDataRecommender() throws IOException, ClassNotFoundException, SQLException{

		Timestamp timeStamp = Timestamp.valueOf("2012-01-01 00:00:0.1");;
		UserApiCSVFileHandler userApiFileHandler = new UserApiCSVFileHandler();
		userApiFileHandler.writeUserApisDataUsedBeforeTimestampToFile(timeStamp);
		//userApiFileHandler.writeUserApiDataToFile();
		userApiIDGeneratorHistorical = new UserApiIDGenerator();

		String inputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "HistoricUserApiData.csv";

		String outputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "HistoricUserApiIDData.csv";



		userApiIDGeneratorHistorical.generateUserApiIDDataFromFile(inputFilePath, outputFilePath);

		apiNameHistoricalMap = new HashMap<Long, String>(userApiIDGeneratorHistorical.getApiNameMap());
		//System.out.println("Map size - "+ apiNameHistoricalMap.size());
		userNameHistoricalMap = userApiIDGeneratorHistorical.getUserNameMap();
		//String userName = "Arser";
		//String userName = "Arser";

		// specifying the user id to which the recommendations have to be generated for

		//specifying the number of recommendations to be generated
	
		//specifying theNeighborhood size
		double thresholdValue=0.1;
		try{

			// Data model created to accept the input file
			FileDataModel dataModel = new FileDataModel(new File("CSVFiles/HistoricUserApiIDData.csv"));

			//GenericDataModel dataModel = new GenericDataModel(userIdMap);

			/*TanimotoCoefficientSimilarity is intended for "binary" data sets
                  where a user either expresses a generic "yes" preference for an item or has no preference.*/
			UserSimilarity userSimilarity = new TanimotoCoefficientSimilarity(dataModel);
			//UserSimilarity userSimilarity = new LogLikelihoodSimilarity(dataModel);

			/*ThresholdUserNeighborhood is preferred in situations where we go in for a
                   similarity measure between neighbors and not any number*/
			//UserNeighborhood neighborhood =new ThresholdUserNeighborhood(thresholdValue, userSimilarity, dataModel);
			UserNeighborhood neighborhood = new NearestNUserNeighborhood(30, userSimilarity, dataModel);

			/*GenericBooleanPrefUserBasedRecommender is appropriate for use when no notion
                  of preference value exists in the data. */
			historicalRecommender = new GenericBooleanPrefUserBasedRecommender(dataModel, neighborhood, userSimilarity);

			//calling the recommend method to generate recommendations
	
			//
		
			//if(recommendations.size() == 0)
			//	System.out.println("Not enough information to generate recommendations");
			//for (RecommendedItem recommendedItem : recommendations)
			//	System.out.println( "Api recommended = " + userApiIDGeneratorHistorical.getApiName(recommendedItem.getItemID()));
			//System.out.println("**********\n\n");
			/*
			DataModel model = new GenericBooleanPrefDataModel(dataModel);

			RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();

			RecommenderBuilder recommenderBuilder = new RecommenderBuilder(){

				public Recommender buildRecommender(DataModel model)
						throws TasteException {

					UserSimilarity similarity = new LogLikelihoodSimilarity(model);
					UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.5, similarity, model);

					return new GenericUserBasedRecommender(model, neighborhood, similarity);
				}

			};

			DataModelBuilder modelBuilder = new DataModelBuilder(){

				public DataModel buildDataModel(
						FastByIDMap<PreferenceArray> trainingData) {
					return new GenericBooleanPrefDataModel(
							GenericBooleanPrefDataModel.toDataMap(trainingData));

				}
			};


			IRStatistics stats = evaluator.evaluate(recommenderBuilder, modelBuilder, model, null, 10, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1.0);
			System.out.println(stats.getPrecision());
			System.out.println(stats.getRecall());*/

		} catch (TasteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<RecommendedItem> getRecommendationsForHistoricalData(String userName, Integer noOfRecommendations) throws TasteException {

	
			/*GenericBooleanPrefUserBasedRecommender is appropriate for use when no notion
                  of preference value exists in the data. */
			List<RecommendedItem> recommendations = null;
			Long userId = userApiIDGeneratorHistorical.getUserId(userName);

			//calling the recommend method to generate recommendations
			recommendations = historicalRecommender.recommend(userId, noOfRecommendations);

			//
			System.out.println("Api recommendations for user : " + userApiIDGeneratorHistorical.getUserName(userId));

			//if(recommendations.size() == 0)
			//	System.out.println("Not enough information to generate recommendations");
			//for (RecommendedItem recommendedItem : recommendations)
			//	System.out.println( "Api recommended = " + userApiIDGeneratorHistorical.getApiName(recommendedItem.getItemID()));
			//System.out.println("**********\n\n");
			/*
			DataModel model = new GenericBooleanPrefDataModel(dataModel);

			RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();

			RecommenderBuilder recommenderBuilder = new RecommenderBuilder(){

				public Recommender buildRecommender(DataModel model)
						throws TasteException {

					UserSimilarity similarity = new LogLikelihoodSimilarity(model);
					UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.5, similarity, model);

					return new GenericUserBasedRecommender(model, neighborhood, similarity);
				}

			};

			DataModelBuilder modelBuilder = new DataModelBuilder(){

				public DataModel buildDataModel(
						FastByIDMap<PreferenceArray> trainingData) {
					return new GenericBooleanPrefDataModel(
							GenericBooleanPrefDataModel.toDataMap(trainingData));

				}
			};


			IRStatistics stats = evaluator.evaluate(recommenderBuilder, modelBuilder, model, null, 10, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1.0);
			System.out.println(stats.getPrecision());
			System.out.println(stats.getRecall());*/

	

		return recommendations;
	}
	
	
	public void initializeRecommenderForLatestData() throws IOException, ClassNotFoundException, SQLException, TasteException{
		
		UserApiCSVFileHandler userApiFileHandler = new UserApiCSVFileHandler();
		//userApiFileHandler.writeUserApisDataUsedBeforeTimestampToFile(timeStamp);
		userApiFileHandler.writeUserApiDataToFile();
		userApiIDGeneratorLatestData = new UserApiIDGenerator();

		String inputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "LatestUserApiData.csv";

		String outputFilePath = System.getProperty("user.dir") 
				+ File.separator + "CSVFiles"
				+ File.separator + "LatestUserApiIDData.csv";




		userApiIDGeneratorLatestData.generateUserApiIDDataFromFile(inputFilePath, outputFilePath);

		userNameLatestMap = userApiIDGeneratorLatestData.getUserNameMap();
		apiNameLatestMap = userApiIDGeneratorLatestData.getApiNameMap();

		//String userName = "Arser";
		//String userName = "Arser";
	//	Long userId = userApiIDGeneratorLatest.getUserId(userName);

		// specifying the user id to which the recommendations have to be generated for

		//specifying the number of recommendations to be generated
		//int noOfRecommendations=20;

		//specifying theNeighborhood size
		double thresholdValue=0.1;
	//	List<RecommendedItem> recommendations = null;
	
			// Data model created to accept the input file
			FileDataModel dataModel = new FileDataModel(new File("CSVFiles/LatestUserApiIDData.csv"));

			//GenericDataModel dataModel = new GenericDataModel(userIdMap);

			/*TanimotoCoefficientSimilarity is intended for "binary" data sets
                  where a user either expresses a generic "yes" preference for an item or has no preference.*/
			//UserSimilarity userSimilarity = new TanimotoCoefficientSimilarity(dataModel);
			UserSimilarity userSimilarity = new LogLikelihoodSimilarity(dataModel);

			/*ThresholdUserNeighborhood is preferred in situations where we go in for a
                   similarity measure between neighbors and not any number*/
			//UserNeighborhood neighborhood =new ThresholdUserNeighborhood(thresholdValue, userSimilarity, dataModel);
			UserNeighborhood neighborhood = new NearestNUserNeighborhood(20, userSimilarity, dataModel);

			/*GenericBooleanPrefUserBasedRecommender is appropriate for use when no notion
                  of preference value exists in the data. */
			latestDataRecommender = new GenericBooleanPrefUserBasedRecommender(dataModel, neighborhood, userSimilarity);

			//calling the recommend method to generate recommendations
			//recommendations =recommender.recommend(userId, noOfRecommendations);

			//
			//if(recommendations.size() == 0)
			//	System.out.println("Not enough information to generate recommendations");
			//for (RecommendedItem recommendedItem : recommendations)
			//	System.out.println( "Api recommended = " + userApiIDGeneratorLatest.getApiName(recommendedItem.getItemID()));
			/*
			DataModel model = new GenericBooleanPrefDataModel(dataModel);

			RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();

			RecommenderBuilder recommenderBuilder = new RecommenderBuilder(){

				public Recommender buildRecommender(DataModel model)
						throws TasteException {

					UserSimilarity similarity = new LogLikelihoodSimilarity(model);
					UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.5, similarity, model);

					return new GenericUserBasedRecommender(model, neighborhood, similarity);
				}

			};

			DataModelBuilder modelBuilder = new DataModelBuilder(){

				public DataModel buildDataModel(
						FastByIDMap<PreferenceArray> trainingData) {
					return new GenericBooleanPrefDataModel(
							GenericBooleanPrefDataModel.toDataMap(trainingData));

				}
			};

			IRStatistics stats = evaluator.evaluate(recommenderBuilder, modelBuilder, model, null, 10, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1.0);
			System.out.println(stats.getPrecision());
			System.out.println(stats.getRecall());*/

	}

	public List<RecommendedItem> getRecommendationsForLatestData(String userName, Integer noOfRecommendations) throws ClassNotFoundException, IOException, SQLException, TasteException{

		//String userName = "Arser";
		//String userName = "Arser";
		Long userId = userApiIDGeneratorLatestData.getUserId(userName);

		// specifying the user id to which the recommendations have to be generated for

		//specifying the number of recommendations to be generated
		
		//specifying theNeighborhood size
		double thresholdValue=0.1;
		List<RecommendedItem> recommendations = null;
		
			//calling the recommend method to generate recommendations
			recommendations = latestDataRecommender.recommend(userId, noOfRecommendations);

			//
			//if(recommendations.size() == 0)
			//	System.out.println("Not enough information to generate recommendations");
			//for (RecommendedItem recommendedItem : recommendations)
			//	System.out.println( "Api recommended = " + userApiIDGeneratorLatest.getApiName(recommendedItem.getItemID()));
			/*
			DataModel model = new GenericBooleanPrefDataModel(dataModel);

			RecommenderIRStatsEvaluator evaluator = new GenericRecommenderIRStatsEvaluator();

			RecommenderBuilder recommenderBuilder = new RecommenderBuilder(){

				public Recommender buildRecommender(DataModel model)
						throws TasteException {

					UserSimilarity similarity = new LogLikelihoodSimilarity(model);
					UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.5, similarity, model);

					return new GenericUserBasedRecommender(model, neighborhood, similarity);
				}

			};

			DataModelBuilder modelBuilder = new DataModelBuilder(){

				public DataModel buildDataModel(
						FastByIDMap<PreferenceArray> trainingData) {
					return new GenericBooleanPrefDataModel(
							GenericBooleanPrefDataModel.toDataMap(trainingData));

				}
			};

			IRStatistics stats = evaluator.evaluate(recommenderBuilder, modelBuilder, model, null, 10, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1.0);
			System.out.println(stats.getPrecision());
			System.out.println(stats.getRecall());*/
		return recommendations;
	}


	/*public static void main(String args[]) throws IOException, ClassNotFoundException, SQLException {

		//PWUserRecommender pwUserRecommender = new PWUserRecommender();
		//pwUserRecommender.generateRecommendations("Arser");
		//pwUserRecommender.generateRecommendationsForOldData("Arser");
	}*/

}