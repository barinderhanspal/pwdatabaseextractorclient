package com.programmableweb.recommender.usercf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;

import com.sun.jersey.impl.ApiMessages;

public class PWUserApiRecommendationEvaluator {

	public void calculatePredictionScore() throws IOException, ClassNotFoundException, SQLException, TasteException{

		UserApiCSVFileHandler userApiCSVFileHandler = new UserApiCSVFileHandler();
		HashMap<String, Set<String>> latestUserApiDataMap = userApiCSVFileHandler.extractLatestUserApiData();
		HashMap<String, Set<String>> historicUserApiDataMap = userApiCSVFileHandler.extractHistoricUserApiData();

		List<Entry<String, Set<String>>> historicUserApiDataList = new LinkedList<Entry<String, Set<String>>>(historicUserApiDataMap.entrySet());
		PWUserRecommender pwUserRecommender = new PWUserRecommender();

		pwUserRecommender.initializeHistoricalDataRecommender();
		pwUserRecommender.initializeRecommenderForLatestData();

		//pwUserRecommender.getRecommendationsForLatestData("", 20);
		//pwUserRecommender.getRecommendationsForHistoricalData("", 20);

		//HashMap<Long, String> userNameHistoricalMap = pwUserRecommender.getUserNameHistoricalMap();
		HashMap<Long, String> apiNameHistoricalMap = pwUserRecommender.getApiNameHistoricalMap();

		Vector<Double> recommendationAccuracyForEachUser = new Vector<Double>(); 

		int userCount = 1;
		for(Entry<String, Set<String>> historicUserApiEntry: historicUserApiDataList){
			
			System.out.println("Count = " + userCount);
			if(latestUserApiDataMap.containsKey(historicUserApiEntry.getKey())){
				Set<String> recommendedApisSet = new HashSet<String>();
				List<RecommendedItem> recommendations = pwUserRecommender.getRecommendationsForHistoricalData(historicUserApiEntry.getKey(), 30);
				for(RecommendedItem recommendedApi : recommendations){
					//System.out.println(apiNameHistoricalMap == null);
					recommendedApisSet.add(apiNameHistoricalMap.get(recommendedApi.getItemID()));
				}


				// newapis = latest apis used by user - historical apis used by user

				Set<String> newApisUsedByUser = new HashSet<String>(latestUserApiDataMap.get(historicUserApiEntry.getKey()));
				newApisUsedByUser.removeAll(historicUserApiDataMap.get(historicUserApiEntry.getKey()));
				System.out.println("New apis = " + newApisUsedByUser);

				Set<String> correctlyRecommendedApisToUser = new HashSet<String>();
				correctlyRecommendedApisToUser.addAll(recommendedApisSet);
				correctlyRecommendedApisToUser.retainAll(newApisUsedByUser);
				System.out.println("recommended apis = " + correctlyRecommendedApisToUser);
				
				if(newApisUsedByUser.size() != 0) {
					userCount++;
					double accuracy = correctlyRecommendedApisToUser.size() / (double) (newApisUsedByUser.size() == 0? 1 : newApisUsedByUser.size());
					recommendationAccuracyForEachUser.add(accuracy);
					System.out.println("Accuracy for this user = " + accuracy);
				}

			} else {
				System.out.println("User : " + historicUserApiEntry.getKey() + " does not exist in the latest after the specified time period");
			}
		}

		double overallAccuracy = 0;
		for(int index = 0; index < recommendationAccuracyForEachUser.size(); index ++)
			overallAccuracy += recommendationAccuracyForEachUser.get(index);

		System.out.println("Accuracy = " + ( overallAccuracy / recommendationAccuracyForEachUser.size()));
	}

	public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException, TasteException {

		PWUserApiRecommendationEvaluator pwUserApiRecommendationEvaluator = new PWUserApiRecommendationEvaluator();
		pwUserApiRecommendationEvaluator.calculatePredictionScore();
	}

}
