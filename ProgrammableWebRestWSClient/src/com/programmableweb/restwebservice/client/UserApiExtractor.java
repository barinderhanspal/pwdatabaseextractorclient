package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class UserApiExtractor {

	private Connection dbConnection = null;

	private HashMap<String, String> getMashupsForUsersFromDB() throws SQLException, ClassNotFoundException{
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();

		HashMap<String, String> mashupsForUserMap = new HashMap<String, String>();

		String memberMashupsSQL = "SELECT user_id, mashups FROM `programmableweb_database`.`members` "
				+ "WHERE mashups IS NOT NULL";
		PreparedStatement memberMashupPS = dbConnection.prepareStatement(memberMashupsSQL);
		ResultSet memberMashups = memberMashupPS.executeQuery();

		//System.out.println("BEGIN... Extracting Mashup created by users from database");

		//System.out.println("*****************************************************************\nuser_id \t\t mashups");
		while(memberMashups.next()){
			//	System.out.println(memberMashups.getString("user_id") + " , "+  memberMashups.getString("mashups"));
			mashupsForUserMap.put(memberMashups.getString("user_id"), memberMashups.getString("mashups"));
		}

		//System.out.println("******************************************************************");
		//System.out.println("COMPLETE... Extracting Mashup created by users from database");
		return mashupsForUserMap;
	}

	private HashMap<String, Set<String>> getApisUsedByMashups() throws ClassNotFoundException, SQLException{
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();

		HashMap<String, Set<String>> apisUsedByMashupMap = new HashMap<String, Set<String>>();

		String retrieveApisUsedByMashupsSQL = "SELECT * FROM `programmableweb_database`.`mashup_api` ";
		PreparedStatement retrieveApisUsedByMashupsPS = dbConnection.prepareStatement(retrieveApisUsedByMashupsSQL);
		ResultSet apisUsedByMashup = retrieveApisUsedByMashupsPS.executeQuery();

		//System.out.println("BEGIN... Extracting Mashup Api relationships from database");
		while(apisUsedByMashup.next()){
			if(apisUsedByMashupMap.containsKey(apisUsedByMashup.getString("mashup_name"))){
				apisUsedByMashupMap.get(apisUsedByMashup.getString("mashup_name")).add(apisUsedByMashup.getString("api_name"));
			} else {
				Set<String> apis = new LinkedHashSet<String>();
				apis.add(apisUsedByMashup.getString("api_name"));
				apisUsedByMashupMap.put(apisUsedByMashup.getString("mashup_name"), apis);
			}
		}
		/*List<Entry<String, Set<String>>> mashupapis = new ArrayList<Entry<String, Set<String>>>(apisUsedByMashupMap.entrySet());

		System.out.println("*****************************************************************\nmashup \t\t apis");
		for(Entry<String, Set<String>> mashupapi : mashupapis){
			System.out.println(mashupapi.getKey() + ":  " + mashupapi.getValue());
		}*/
		//System.out.println("COMPLETE... Extracting Mashup Api relationships from database");

		return apisUsedByMashupMap;
	}

	private HashMap<String, Set<String>> getApisUsedByMashups(Timestamp timestamp) throws ClassNotFoundException, SQLException{
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();

		HashMap<String, Set<String>> apisUsedByMashupMap = new HashMap<String, Set<String>>();

		String retrieveApisUsedByMashupsSQL = "SELECT * FROM `programmableweb_database`.`mashup_api` ";
		PreparedStatement retrieveApisUsedByMashupsPS = dbConnection.prepareStatement(retrieveApisUsedByMashupsSQL);
		ResultSet apisUsedByMashup = retrieveApisUsedByMashupsPS.executeQuery();

		String retrieveMashupCreationTimeSQL = "SELECT mashup_name, date_modified from mashups";
		PreparedStatement retrieveMashupCreationTimePS = dbConnection.prepareStatement(retrieveMashupCreationTimeSQL);
		ResultSet mashupsCreationResultSet = retrieveMashupCreationTimePS.executeQuery();

		HashMap<String, Timestamp> mashupCreationTimeStampMap = new HashMap<String, Timestamp>();

		while(mashupsCreationResultSet.next()){
			mashupCreationTimeStampMap.put(mashupsCreationResultSet.getString("mashup_name"),
					mashupsCreationResultSet.getTimestamp("date_modified"));		
		}

		//System.out.println("BEGIN... Extracting Mashup Api relationships from database");
		while(apisUsedByMashup.next()){
			if(mashupCreationTimeStampMap.containsKey(apisUsedByMashup.getString("mashup_name"))) {
				if(mashupCreationTimeStampMap.get(apisUsedByMashup.getString("mashup_name")).before(timestamp)){
					if(apisUsedByMashupMap.containsKey(apisUsedByMashup.getString("mashup_name"))){
						apisUsedByMashupMap.get(apisUsedByMashup.getString("mashup_name")).add(apisUsedByMashup.getString("api_name"));
					} else {
						Set<String> apis = new LinkedHashSet<String>();
						apis.add(apisUsedByMashup.getString("api_name"));
						apisUsedByMashupMap.put(apisUsedByMashup.getString("mashup_name"), apis);
					}
				}
			}
		}
		/*List<Entry<String, Set<String>>> mashupapis = new ArrayList<Entry<String, Set<String>>>(apisUsedByMashupMap.entrySet());

		System.out.println("*****************************************************************\nmashup \t\t apis");
		for(Entry<String, Set<String>> mashupapi : mashupapis){
			System.out.println(mashupapi.getKey() + ":  " + mashupapi.getValue());
		}*/
		//System.out.println("COMPLETE... Extracting Mashup Api relationships from database");

		return apisUsedByMashupMap;
	}

	private HashMap<String, Set<String>> extractAllApisUsedByUsers(HashMap<String, String> mashupsUsedByUserMap, HashMap<String, Set<String>> apisUsedByMashupsMap) {

		//System.out.println("Creating User-Api Map...");
		HashMap<String, Set<String>> apisUsedByAllUsersMap = new HashMap<String, Set<String>>();

		List<Entry<String, String>> mashupsUsedByUsersList = new ArrayList<Entry<String, String>>(mashupsUsedByUserMap.entrySet());
		//System.out.println("********");
		//System.out.println(mashupsUsedByUserMap.entrySet());
		//System.out.println("********");

		// For each user < "userid", "mashup1, mashup2,..."> 

		for(Entry<String, String> mashupsUsedByUserEntry : mashupsUsedByUsersList){

			//System.out.println("*************************************");
			// Store all mashups used by the user into a set
			//System.out.println("User name = " + mashupsUsedByUserEntry.getKey());

			Set<String> mashupsUsedByCurrentUser = new HashSet<String>(Arrays.asList(mashupsUsedByUserEntry.getValue().split(",")));
			//System.out.println("Mashups created by user = " + mashupsUsedByCurrentUser);

			//System.out.println("mashups set for "+mashupsUsedByUserEntry.getKey() + " = " + mashupsUsedByUserEntry.getValue());
			// Stores all mashups used by the user into a list for easy traversal
			List<String> mashupsUsedByCurrentUserList = new ArrayList<String>(mashupsUsedByCurrentUser);

			// Apis used by the current user
			Set<String> apisUsedByCurrentUser = new HashSet<String>();

			// for each mashup
			//System.out.println("\n*********** \nUser = " + mashupsUsedByUserEntry.getKey());
			for(String mashupUsedByCurrentUser : mashupsUsedByCurrentUserList){
				//	System.out.println("Mashup name : " + mashupUsedByCurrentUser);
				//	System.out.println("Apis used by this mashup : " + apisUsedByMashupsMap.get(mashupUsedByCurrentUser));
				//				System.out.println("apisUsedByCurrentUser : " + mashupUsedByCurrentUser);
				//				System.out.println(apisUsedByMashupsMap.get(mashupUsedByCurrentUser));
				//				System.out.println("mashupsused : " + mashupsUsedByUserMap.get(mashupUsedByCurrentUser));
				if(apisUsedByMashupsMap.containsKey(mashupUsedByCurrentUser)){
					apisUsedByCurrentUser.addAll(apisUsedByMashupsMap.get(mashupUsedByCurrentUser));
				}
			}

			// for each mashup find the apis used and store into the apisForUser set
			apisUsedByAllUsersMap.put(mashupsUsedByUserEntry.getKey(), apisUsedByCurrentUser);


		}

		//System.out.println("User-Api Map Created...");
		return apisUsedByAllUsersMap;
	}



	public HashMap<String, Set<String>> getAllApisUsedByUsers() throws ClassNotFoundException, SQLException{

		return extractAllApisUsedByUsers(getMashupsForUsersFromDB(), getApisUsedByMashups());
	}

	public HashMap<String, Set<String>> getAllApisUsedByUsers(Timestamp beforeTimeStamp) throws ClassNotFoundException, SQLException{

		return extractAllApisUsedByUsers(getMashupsForUsersFromDB(), getApisUsedByMashups(beforeTimeStamp));
	}

	/*	public static void main(String args[]) throws ClassNotFoundException, SQLException{

		UserApiRelationshipFinder finder = new UserApiRelationshipFinder();
		List<Entry<String, Set<String>>> userApiList = new ArrayList<Entry<String, Set<String>>>(finder.getAllApisUsedByUsers().entrySet());

		for(Entry<String, Set<String>> userApiEntry : userApiList)
			System.out.println("User_id = " + userApiEntry.getKey() + "\n" + "Api = " + userApiEntry.getValue() );
	}*/
}

