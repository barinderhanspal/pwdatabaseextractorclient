package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class PWApiDatabaseUpdater {

	Connection dbConnection = null;
	
	public PWApiDatabaseUpdater(){}
	
	public void updateApiDatabase(List<PWApi> apiList) throws ClassNotFoundException, SQLException{
		
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();
		
		HashMap<String, Integer> categoryMap = new HashMap<String, Integer>();
		
		String retrieveCategoriesSQL = "SELECT * FROM `programmableweb_database`.`categories`";
		PreparedStatement catSQL = dbConnection.prepareStatement(retrieveCategoriesSQL);
		ResultSet categories = catSQL.executeQuery();
		
		while(categories.next()){
			categoryMap.put(categories.getString("category_name"), categories.getInt("category_id"));
		}
		
		String insertSQL = "INSERT INTO `programmableweb_database`.`apis` (`api_id`, `title`, `author_name`,"
				+ " `summary`, `rating`, `description`, `downloads`, `use_count`, `date_modified`, `remote_feed`, "
				+ "`num_comments`, `tags`, `category_id`, `protocol`, `data_formats`, `commercial`, `managed_by`,"
				+ " `data_licencing`, `fees`, `limits`, `terms`, `company`, `api_name`) "
				+ "VALUES"
				+ " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
				+ "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		
		String insertTagSQL = "INSERT INTO `programmableweb_database`.`api_tags` (`api_name`, `tag`) "
				+ "VALUES"
				+ " (?, ?);";
		
		PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
		PreparedStatement preparedStatementForTag = dbConnection.prepareStatement(insertTagSQL);
		int count = 0;
		
		for(PWApi api: apiList){
		
			preparedStatement.setString(1, api.getApiId());
			preparedStatement.setString(2, api.getTitle());
			preparedStatement.setString(3, api.getAuthorName());
			preparedStatement.setString(4, api.getSummary());
			preparedStatement.setFloat(5, api.getRating());
			preparedStatement.setString(6, api.getDescription());
			preparedStatement.setInt(7, api.getDownloads());
			preparedStatement.setInt(8, api.getUseCount());
			preparedStatement.setTimestamp(9, api.getDateModified());
			preparedStatement.setString(10, api.getRemoteFeed());
			preparedStatement.setInt(11, api.getNumComments());
			for(String tag : api.getTags()){
				preparedStatementForTag.setString(1, api.getApiName());
				preparedStatementForTag.setString(2, tag);
				preparedStatementForTag.addBatch();
			}
			preparedStatement.setString(12, api.getTagsForDatabase());
			//System.out.println( "api.getcate" + api.getCategory());
			//System.out.println("categiry map " + categoryMap.get(api.getCategory()));
			preparedStatement.setInt(13, categoryMap.get(api.getCategory()));
			preparedStatement.setString(14, api.getProtocol());
			preparedStatement.setString(15, api.getDataFormat());
			preparedStatement.setString(16, api.getCommercial());
			preparedStatement.setString(17, api.getManagedBy());
			preparedStatement.setString(18, api.getDataLicencing());
			preparedStatement.setString(19, api.getFees());
			preparedStatement.setString(20, api.getLimits());
			preparedStatement.setString(21, api.getTerms());
			preparedStatement.setString(22, api.getCompany());
			preparedStatement.setString(23, api.getApiName());
			
			preparedStatement.addBatch();
			++count;
		
			if( (count != 0) && (count % 500 == 0) ){
				System.out.println("Database insertion count = " + count);
				preparedStatementForTag.executeBatch();
				preparedStatement.executeBatch();	
			}
		}
		System.out.println("Database insertion count = " + count);
		preparedStatementForTag.executeBatch();
		preparedStatement.executeBatch();
		
		dbConnection.commit();
		dbConnection.close();
	}
}
