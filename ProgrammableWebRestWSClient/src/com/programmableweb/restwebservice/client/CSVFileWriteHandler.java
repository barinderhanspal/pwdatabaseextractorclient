/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.io.File;

/**
 * @author BarinderSingh
 *
 */
public class CSVFileWriteHandler {
	
	private static final String outputFilePath = System.getProperty("user.dir") 
													+ File.separator + "CSVFiles"
													+ File.separator + "UserApiData.csv";
	private static volatile File outputFile = null;
	
	private CSVFileWriteHandler(){}
	
	public static File getFileInstance(){
		if(outputFile == null)
			synchronized(CSVFileWriteHandler.class){
				if(outputFile == null)
					outputFile = new File(outputFilePath);
			}
		return outputFile;
	}
	
	/*public static void main(String args[]) throws IOException{
		File op = getFileInstance();
		FileWriter fr = new FileWriter(outputFile);
		BufferedWriter bw = new BufferedWriter(fr);
		bw.write("hello");
		bw.newLine();
		bw.write("hello");
		bw.close();
		fr.close();
	}*/
}
