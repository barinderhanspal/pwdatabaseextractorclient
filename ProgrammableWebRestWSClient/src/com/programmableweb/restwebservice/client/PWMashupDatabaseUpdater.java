package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class PWMashupDatabaseUpdater {

	Connection dbConnection = null;
	
	public PWMashupDatabaseUpdater(){}
	
	public void updateMashupDatabase(List<PWMashup> mashupList) throws ClassNotFoundException, SQLException{
		
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();
			
		String insertSQL = "INSERT INTO `programmableweb_database`.`mashups` (`mashup_id`, `mashup_name`,"
				+ " `author_name`, `summary`, `rating`, `description`, `use_count`, `num_comments`, `tags`,"
				+ " `apis`, `updated`, `date_modified`) "
				+ " VALUES"
				+ " (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
				+ " ?, ?);";
		
		String insertTagSQL = "INSERT INTO `programmableweb_database`.`mashup_tags` (`mashup_name`, `tag`) "
				+ "VALUES"
				+ " (?, ?);";
		

		String insertMashupApiSQL = "INSERT INTO `programmableweb_database`.`mashup_api` (`mashup_name`, `api_name`) "
				+ "VALUES"
				+ " (?, ?);";
		
		PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
		PreparedStatement preparedStatementForTag = dbConnection.prepareStatement(insertTagSQL);
		PreparedStatement preparedStatementForMashupApi = dbConnection.prepareStatement(insertMashupApiSQL);
		
		int count = 0;
		
		for(PWMashup mashup: mashupList){
		
			preparedStatement.setString(1, mashup.getMashupId());
			preparedStatement.setString(2, mashup.getMashupName());
			preparedStatement.setString(3, mashup.getAuthorName());
			preparedStatement.setString(4, mashup.getSummary());
			preparedStatement.setFloat(5, mashup.getRating());
			preparedStatement.setString(6, mashup.getDescription());
			preparedStatement.setInt(7, mashup.getUseCount());
			preparedStatement.setInt(8, mashup.getNumComments());
			preparedStatement.setString(9, mashup.getTagsForDatabase());
			preparedStatement.setString(10, mashup.getApisForDatabase());
			preparedStatement.setTimestamp(11, mashup.getUpdated());
			preparedStatement.setTimestamp(12, mashup.getDateModified());
			for(String tag : mashup.getTags()){
				preparedStatementForTag.setString(1, mashup.getMashupName());
				preparedStatementForTag.setString(2, tag);
				preparedStatementForTag.addBatch();
			}
			for(String api : mashup.getApis()){
				preparedStatementForMashupApi.setString(1, mashup.getMashupName());
				preparedStatementForMashupApi.setString(2, api);
				preparedStatementForMashupApi.addBatch();
			}
			
			preparedStatement.addBatch();
			++count;
		
			if( (count != 0) && (count % 500 == 0) ){
				System.out.println("Database insertion count = " + count);
				
				preparedStatementForTag.executeBatch();
				preparedStatementForMashupApi.executeBatch();
				preparedStatement.executeBatch();	
			}
		}
		System.out.println("Database insertion count = " + count);
		preparedStatementForTag.executeBatch();
		preparedStatementForMashupApi.executeBatch();
		preparedStatement.executeBatch();
		
		dbConnection.commit();
		dbConnection.close();
	}
}
