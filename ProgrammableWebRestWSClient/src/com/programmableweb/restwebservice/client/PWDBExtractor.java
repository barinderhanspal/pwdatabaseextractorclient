/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * @author BarinderSingh
 *
 */
public class PWDBExtractor {

	PWDBExtractorWSClient pwClient;
	PWParser pwParser;

	public PWDBExtractor() throws ParserConfigurationException, SAXException{	
		pwClient = new PWDBExtractorWSClient();
		pwParser = new PWParser();
	}



	public List<PWMember> extractAllPWMembers() throws SAXException, IOException{

		List<PWMember> pwMembers = new LinkedList<PWMember>();
		int pageNumber = 1, itemsPerPage = 100;
		String xmlResponse = null;

		do{
			xmlResponse = pwClient.getMemberXMLData(pageNumber, itemsPerPage);
			pwMembers.addAll(pwParser.extractPWMembers(xmlResponse));
			System.out.println("Processing page no. : " + pageNumber);
			++pageNumber;

		}while(pwParser.hasMoreMembers());

		System.out.println("Total number of members extracted = " + pwMembers.size());

		int memberCount = 0, totalMembers = pwMembers.size();
		for(PWMember member : pwMembers){
			int mashupItemsPerPage = 10;
			int mashupPageNumber = 1;

			do{
				xmlResponse = pwClient.getMemberMashupsCreated(member.getUserId(), mashupPageNumber, mashupItemsPerPage);
				List <PWMashup> pwMashups = new LinkedList<PWMashup>();
				pwMashups.addAll(pwParser.extractPWMashups(xmlResponse));

				String mashups = "";
				for(PWMashup mashup: pwMashups){
					mashups += (mashup.getMashupName() + ",");
				}
				if(mashups.length() != 0) {
					mashups = mashups.substring(0, mashups.length()-1);
					member.setMashups(mashups);
				}

				//System.out.println("Mashups_Created for user \' " + member.getUserId() 
				//			+ "\' : " + mashups);
				++mashupPageNumber;

			}while(pwParser.hasMoreMashups());

			memberCount++;
			if(memberCount != 0 && memberCount % 50 == 0)
				System.out.println("Member Mashups processed : " + memberCount + "/" + totalMembers + "...");

		}

		return pwMembers;
	}

	public List<PWApi> extractAllApis() throws SAXException, IOException{

		List <PWApi> pwApis = new LinkedList<PWApi>();
		int pageNumber = 1, itemsPerPage = 50;
		String xmlResponse = null;

		do{
			xmlResponse = pwClient.getApiXMLData(pageNumber, itemsPerPage);
			pwApis.addAll(pwParser.extractPWApis(xmlResponse));
			System.out.println("Processing page no. : " + pageNumber);
			++pageNumber;

		}while(pwParser.hasMoreApis() );

		System.out.println("Total Number of Apis extracted = " + pwApis.size());


		return pwApis;
	}

	public List<PWMashup> extractAllMashups() throws SAXException, IOException{

		List <PWMashup> pwMashups = new LinkedList<PWMashup>();
		int pageNumber = 1, itemsPerPage = 50;
		String xmlResponse = null;

		do{
			xmlResponse = pwClient.getMashupXMLData(pageNumber, itemsPerPage);
			pwMashups.addAll(pwParser.extractPWMashups(xmlResponse));
			System.out.println("Processing page no. : " + pageNumber);
			++pageNumber;

		}while(pwParser.hasMoreMashups());

		System.out.println("Total Number of Mashups extracted = " + pwMashups.size());

		return pwMashups;
	}

	public static void main (String args[]) throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException{

		PWDBExtractor pw = new PWDBExtractor();
		/*	PWMemberDatabaseUpdater memberDatabase = new PWMemberDatabaseUpdater();
		long startTime = System.currentTimeMillis();
		memberDatabase.updateMemberDatabase(pw.extractAllPWMembers());
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken = " + ((endTime - startTime)/1000) + " seconds");*/

		/*PWApiDatabaseUpdater apiDatabase = new PWApiDatabaseUpdater();
		long startTime = System.currentTimeMillis();
		apiDatabase.updateApiDatabase(pw.extractAllApis());
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken = " + ((endTime - startTime)/1000) + " seconds");
		 */
		PWMashupDatabaseUpdater mashupDatabase = new PWMashupDatabaseUpdater();
		long startTime = System.currentTimeMillis();
		mashupDatabase.updateMashupDatabase(pw.extractAllMashups());
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken = " + ((endTime - startTime)/1000) + " seconds");
	}

}
