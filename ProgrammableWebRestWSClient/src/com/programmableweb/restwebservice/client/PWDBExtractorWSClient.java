/**
 * 
 */
package com.programmableweb.restwebservice.client;

/**
 * @author BarinderSingh
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class PWDBExtractorWSClient {


	private static final String APIKEY = "185cd955366dd133313b12ab00b84a36";
	private static final String URL = "http://api.programmableweb.com/";
	
	public String getMemberMashupsCreated(String memberName, int pageNumber, int itemsPerPage){
		String output = "";
		try {

			String targetURL = URL + "members/" + URLEncoder.encode(memberName.replace("#", ""), "ISO-8859-1") + "/mashups?pagesize=" + itemsPerPage + "&page=" 
								+ pageNumber + "&apikey=" + APIKEY; 
			
		/*	String targetURL = URL + "members/" + URLUtils.encodePathSegment(memberName, "UTF-8") + "/mashups?pagesize=" + itemsPerPage + "&page=" 
					+ pageNumber + "&apikey=" + APIKEY; */

			//System.out.println(targetURL);
			URL restServiceURL = new URL(targetURL);

			HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.setRequestProperty("Accept", "application/json");

			if (httpConnection.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : "
						+ httpConnection.getResponseCode());
			}
			
			BufferedReader responseBuffer = new BufferedReader(
												new InputStreamReader(
														(httpConnection.getInputStream()), "UTF-8"
												)
											);
			String line;
			//System.out.println("Reading response for member : " + memberName);
			//int count = 0;
			while ((line = responseBuffer.readLine()) != null) {
				output += (line.replaceAll("[\\000]*", "").replaceAll( "& *", "&amp;" ) + "\n");

				/*if(count % 10000 == 0){
					System.out.println("Lines read = " + count);
				}
				count++;*/
			}
			responseBuffer.close();
			//output = output.replaceAll("[\\000]*", "").replaceAll("&  *", "&amp; ");
			httpConnection.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return output;
	}
	
	public String getMemberXMLData(int pageNumber, int itemsPerPage){
		
		String output = "";
		try {

			String targetURL = URL + "members?pagesize=" + itemsPerPage + "&page=" 
								+ pageNumber + "&apikey=" + APIKEY; 
			//System.out.println(targetURL);
			URL restServiceURL = new URL(targetURL);

			HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.setRequestProperty("Accept", "application/json");

			if (httpConnection.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : "
						+ httpConnection.getResponseCode());
			}
			
			BufferedReader responseBuffer = new BufferedReader(
												new InputStreamReader(
														(httpConnection.getInputStream()), "UTF-8"
												)
											);

			String line;
			while ((line = responseBuffer.readLine()) != null) {
				output += (line + "\n");
			}
			output = output.replaceAll("[\\000]*", "");
			httpConnection.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return output;
	}
	
	public String getApiXMLData(int pageNumber, int itemsPerPage){
		
		String output = "";
		try {

			String targetURL = URL + "apis?pagesize=" + itemsPerPage + "&page=" 
								+ pageNumber + "&apikey=" + APIKEY; 
			//System.out.println(targetURL);
			URL restServiceURL = new URL(targetURL);

			HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.setRequestProperty("Accept", "application/json");

			if (httpConnection.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : "
						+ httpConnection.getResponseCode());
			}
			
			BufferedReader responseBuffer = new BufferedReader(
												new InputStreamReader(
														(httpConnection.getInputStream()), "UTF-8"
												)
											);

			String line;
			while ((line = responseBuffer.readLine()) != null) {
				output += (line + "\n");
			}
			output = output.replaceAll("[\\000]*", "").replaceAll("&", "&amp;");
			httpConnection.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return output;
	}
	
public String getMashupXMLData(int pageNumber, int itemsPerPage){
		
		String output = "";
		try {

			String targetURL = URL + "mashups?pagesize=" + itemsPerPage + "&page=" 
								+ pageNumber + "&apikey=" + APIKEY; 
			//System.out.println(targetURL);
			URL restServiceURL = new URL(targetURL);

			HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
			httpConnection.setRequestMethod("GET");
			httpConnection.setRequestProperty("Accept", "application/json");

			if (httpConnection.getResponseCode() != 200) {
				throw new RuntimeException("HTTP GET Request Failed with Error code : "
						+ httpConnection.getResponseCode());
			}
			
			BufferedReader responseBuffer = new BufferedReader(
												new InputStreamReader(
														(httpConnection.getInputStream()), "UTF-8"
												)
											);

			String line;
			while ((line = responseBuffer.readLine()) != null) {
				output += (line + "\n");
			}
			output = output.replaceAll("[\\000]*", "").replaceAll("&", "&amp;");
			httpConnection.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return output;
	}

	/*public static void main(String[] args) {

		PWDBExtractorWSClient test = new PWDBExtractorWSClient();
		System.out.println(test.getMemberXMLData(1, 100));
	}*/
}

