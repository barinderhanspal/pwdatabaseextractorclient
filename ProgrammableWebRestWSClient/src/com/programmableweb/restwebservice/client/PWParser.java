/**
 * 
 */
package com.programmableweb.restwebservice.client;


import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * @author BarinderSingh
 *
 */
public class PWParser {

	SAXParserFactory parserFactory;
	SAXParser parser;
	PWMemberSAXHandler memberHandler;
	PWApiSAXHandler apiHandler;
	PWMashupSAXHandler mashupHandler;

	public PWParser() throws ParserConfigurationException, SAXException {
		parserFactory = SAXParserFactory.newInstance();
		parser = parserFactory.newSAXParser();
		memberHandler = null;
		apiHandler = null;
		mashupHandler = null;
	}

	public boolean hasMoreMembers(){
		return memberHandler.hasMoreMembers();
	}

	public boolean hasMoreApis(){
		return apiHandler.hasMoreApis();
	}
	
	public boolean hasMoreMashups(){
		return mashupHandler.hasMoreMashups();
	}

	public List<PWMember> extractPWMembers(String xmlData) throws SAXException, IOException{

		memberHandler = new PWMemberSAXHandler();
		parser.parse(new InputSource(new StringReader(xmlData)), memberHandler);

		return memberHandler.memberList;
	}

	public List<PWApi> extractPWApis(String xmlData) throws SAXException, IOException{

		apiHandler = new PWApiSAXHandler();
		parser.parse(new InputSource(new StringReader(xmlData)), apiHandler);

		return apiHandler.apiList;
	}

	public List<PWMashup> extractPWMashups(String xmlData) throws SAXException, IOException{

		mashupHandler = new PWMashupSAXHandler();
		parser.parse(new InputSource(new StringReader(xmlData)), mashupHandler);

		return mashupHandler.mashupList;
	}

	/*public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException{
		PWParser parser = new PWParser();
		//parser.extractPWMembers();
	}*/
}
