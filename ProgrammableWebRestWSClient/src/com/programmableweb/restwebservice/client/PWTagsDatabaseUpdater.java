package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class PWTagsDatabaseUpdater {

	Connection dbConnection = null;
	
	public PWTagsDatabaseUpdater(){}
	
	public void updateTagDatabaseTable(List<String> tagsList) throws ClassNotFoundException, SQLException{
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();
		String insertSQL = "INSERT INTO `programmableweb_database`.`tags` (`tag`)"
				+ " VALUES (?)";
		
		PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
		int count = 0;
		
		for(String tag: tagsList){
		
			preparedStatement.setString(1, tag);
			preparedStatement.addBatch();
			++count;
		}
		
		System.out.println("Inserting tags in database. Tag insertion count = " + count);
		preparedStatement.executeBatch();	
		dbConnection.commit();
		System.out.println("Tags committed to database table");
		dbConnection.close();
	}
}
