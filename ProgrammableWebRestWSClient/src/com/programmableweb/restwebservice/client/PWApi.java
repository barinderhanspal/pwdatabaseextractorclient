/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.sql.Timestamp;
import java.util.Set;

/**
 * @author BarinderSingh
 *
 */
public class PWApi {

	private String apiId;
	private String title;
	private String apiName;
	

	private String authorName;
	private String summary;
	private float rating;
	private String description;
	private int downloads;
	private int useCount;
	private Timestamp dateModified;
	private String remoteFeed;
	private int numComments;
	private String commentsURL;
	private Set<String> tags;
	private String category;
	private String dataFormat;
	private String protocol;
	private String commercial;
	private String managedBy;
	private String dataLicencing;
	private String fees;
	private String limits;
	private String terms;
	private String company;
	private Timestamp updated;
	
	/**
	 * @return the apiId
	 */
	public String getApiId() {
		return apiId;
	}

	/**
	 * @param apiId the apiId to set
	 */
	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the authorName
	 */
	public String getauthorName() {
		return authorName;
	}

	/**
	 * @param authorName the authorName to set
	 */
	public void setauthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param summary the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @return the api_name
	 */
	public String getApiName() {
		return apiName;
	}

	/**
	 * @param api_name the api_name to set
	 */
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	/**
	 * @return the authorName
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * @param authorName the authorName to set
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	/**
	 * @return the rating
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the downloads
	 */
	public int getDownloads() {
		return downloads;
	}

	/**
	 * @param downloads the downloads to set
	 */
	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	/**
	 * @return the useCount
	 */
	public int getUseCount() {
		return useCount;
	}

	/**
	 * @param useCount the useCount to set
	 */
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	/**
	 * @return the dateModified
	 */
	public Timestamp getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the remoteFeed
	 */
	public String getRemoteFeed() {
		return remoteFeed;
	}

	/**
	 * @param remoteFeed the remoteFeed to set
	 */
	public void setRemoteFeed(String remoteFeed) {
		this.remoteFeed = remoteFeed;
	}

	/**
	 * @return the numComments
	 */
	public int getNumComments() {
		return numComments;
	}

	/**
	 * @param numComments the numComments to set
	 */
	public void setNumComments(int numComments) {
		this.numComments = numComments;
	}

	/**
	 * @return the commentsURL
	 */
	public String getCommentsURL() {
		return commentsURL;
	}

	/**
	 * @param commentsURL the commentsURL to set
	 */
	public void setCommentsURL(String commentsURL) {
		this.commentsURL = commentsURL;
	}

	/**
	 * @return the tags
	 */
	public Set<String> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the dataFormat
	 */
	public String getDataFormat() {
		return dataFormat;
	}

	/**
	 * @param dataFormat the dataFormat to set
	 */
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

	/**
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return the commercial
	 */
	public String getCommercial() {
		return commercial;
	}

	/**
	 * @param commercial the commercial to set
	 */
	public void setCommercial(String commercial) {
		this.commercial = commercial;
	}

	/**
	 * @return the managedBy
	 */
	public String getManagedBy() {
		return managedBy;
	}

	/**
	 * @param managedBy the managedBy to set
	 */
	public void setManagedBy(String managedBy) {
		this.managedBy = managedBy;
	}

	/**
	 * @return the dataLicencing
	 */
	public String getDataLicencing() {
		return dataLicencing;
	}

	/**
	 * @param dataLicencing the dataLicencing to set
	 */
	public void setDataLicencing(String dataLicencing) {
		this.dataLicencing = dataLicencing;
	}

	/**
	 * @return the fees
	 */
	public String getFees() {
		return fees;
	}

	/**
	 * @param fees the fees to set
	 */
	public void setFees(String fees) {
		this.fees = fees;
	}

	/**
	 * @return the limits
	 */
	public String getLimits() {
		return limits;
	}

	/**
	 * @param limits the limits to set
	 */
	public void setLimits(String limits) {
		this.limits = limits;
	}

	/**
	 * @return the terms
	 */
	public String getTerms() {
		return terms;
	}

	/**
	 * @param terms the terms to set
	 */
	public void setTerms(String terms) {
		this.terms = terms;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the registrationDate
	 */
	public Timestamp getUpdated() {
		return updated;
	}

	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}


	
	public PWApi() {}
		
	
	public String getTagsForDatabase(){
		String tags = "";
		boolean flag = false;
		for(String tag: this.tags){
			tags += (tag + ",");
			flag = true;
		}
		if(flag == true)
			tags = tags.substring(0, tags.length()-1);
		
		return tags;
	}
	
	@Override
	public String toString(){
		return "{userId = " + this.apiId + "\n"
				+ "title = " + this.title + "\n"
				+ "authorName = " + this.authorName + "\n"
				+ "summary = " + this.summary + "\n"
				+ "dateModified = " + this.dateModified + "}";
	}
	
}
