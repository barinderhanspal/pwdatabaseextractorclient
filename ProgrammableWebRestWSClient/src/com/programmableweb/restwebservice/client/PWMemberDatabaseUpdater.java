package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class PWMemberDatabaseUpdater {

	Connection dbConnection = null;
	
	public PWMemberDatabaseUpdater(){}
	
	public void updateMemberDatabase(List<PWMember> memberList) throws ClassNotFoundException, SQLException{
		dbConnection = MySQLDatabaseConnectionHandler.getMySQLDBConnectionInstance();
		String insertSQL = "INSERT INTO `programmableweb_database`.`members` (`user_id`, `profile_url`, "
				+ "`longitude`, `latitude`, `location`, `country`, `registration_date`, `mashups`, `favorites`, `friends`)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
		int count = 0;
		System.out.println("Begin insert members into database...");
		for(PWMember member: memberList){
		
			preparedStatement.setString(1, member.getUserId());
			preparedStatement.setString(2, member.getProfileUrl());
			preparedStatement.setFloat(3, member.getLongitude());
			preparedStatement.setFloat(4, member.getLatitude());
			preparedStatement.setString(5, member.getLocation());
			preparedStatement.setString(6, member.getCountry());
			preparedStatement.setTimestamp(7, member.getRegistrationDate());
			preparedStatement.setString(8, member.getMashups());
			preparedStatement.setString(9, member.getFavorites());
			preparedStatement.setString(10, member.getFriends());
			preparedStatement.addBatch();
			++count;
			
			if( (count != 0) && (count % 500 == 0) ){
				System.out.println("Database insertion count = " + count);
				preparedStatement.executeBatch();	
			}
			//System.out.println("Mashups user: " + member.getUserId() + "\nMashup name: " + member.getMashups());
		}

		preparedStatement.executeBatch();	
		dbConnection.commit();
		dbConnection.close();
		System.out.println("SUCCESS..Database Insertion complete...");
	}
}
