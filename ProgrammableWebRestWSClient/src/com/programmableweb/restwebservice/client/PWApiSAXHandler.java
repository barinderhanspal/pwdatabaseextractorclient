/**
 * 
 */
package com.programmableweb.restwebservice.client;


import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author BarinderSingh
 *
 */
public class PWApiSAXHandler extends DefaultHandler{

	List<PWApi> apiList;
	PWApi api;
	String content;
	int totalResults;
	int startIndex;
	int itemsPerPage;
	boolean authorNameFlag;

	public PWApiSAXHandler(){
		apiList = new LinkedList<PWApi>();
		api = null;
		content = null;
		totalResults = 0;
		startIndex = 0;
		itemsPerPage = 0;
		authorNameFlag = false;
	}

	/**
	 * @param totalResults the totalResults to set
	 */
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * @param itemsPerPage the itemsPerPage to set
	 */
	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * @return the totalResults
	 */
	public int getTotalResults() {
		return totalResults;
	}

	/**
	 * @return the startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * @return the itemsPerPage
	 */
	public int getItemsPerPage() {
		return itemsPerPage;
	}

	@Override
	//Triggered when the start of tag is found.
	public void startElement(String uri, String localName, 
			String qName, Attributes attributes) 
					throws SAXException {

		switch(qName){
		//Create a new member object when the start tag is found
		case "entry":
			api = new PWApi();
			break;
		case "author":
			authorNameFlag = true;
			break;
		case "tags":
			if(api != null){
				api.setTags(new LinkedHashSet<String>());
			}
		default:
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, 
			String qName) throws SAXException {
		switch(qName){
		//Add the employee to list once end tag is found
		case "entry":
			apiList.add(api);     
			break;
			//For all other end tags the employee has to be updated.
		case "id":
			if(api != null){
				api.setApiId(content);
				api.setApiName(content.replaceAll(".*/api/", ""));
			}
			break;
		case "title":
			if(api != null) 
				api.setTitle(content);
			break;
		case "author":
			if(api != null)
				authorNameFlag = false;
			break;
		case "name":
			if(api != null && authorNameFlag)
				api.setauthorName(content);
			break;
		case "summary":
			if(api != null)
				api.setSummary(content);
			break;

		case "rating":
			if(api != null){
				if(content == null || content.equals(""))
					content = "0.0";
				api.setRating(Float.parseFloat(content));
			}
			break;

		case "description":
			if(api != null)
				api.setDescription(content);
			break;

		case "downloads":
			if(api != null){
				if(content == null || content.equals(""))
					content = "0";
				api.setDownloads(Integer.parseInt(content));
			}
			break;

		case "useCount":
			if(api != null){
				if(content == null || content.equals(""))
					content = "0";
				api.setUseCount(Integer.parseInt(content));
			}
			break;

		case "dateModified":
			if(api != null)
				api.setDateModified(Timestamp.valueOf(content.replace("T"," ").replace("Z","")));
			break;

		case "remoteFeed":
			if(api != null)
				api.setRemoteFeed(content);
			break;

		case "numComments":
			if(api != null){
				if(content == null || content.equals(""))
					content = "0";
				api.setNumComments(Integer.parseInt(content));
			}
			break;

		case "tag":
			if(api != null)
				api.getTags().add(content);
			break;

		case "category":
			if(api != null)
				api.setCategory(content);
			break;

		case "protocol":
			if(api != null)
				api.setProtocol(content);
			break;

		case "dataFormats":
			if(api != null)
				api.setDataFormat(content);
			break;

		case "commercial":
			if(api != null)
				api.setCommercial(content);
			break;

		case "managedBy":
			if(api != null)
				api.setManagedBy(content);
			break;

		case "dataLicencing":
			if(api != null)
				api.setDataLicencing(content);
			break;

		case "fees":
			if(api != null)
				api.setFees(content);
			break;

		case "limits":
			if(api != null)
				api.setLimits(content);
			break;

		case "terms":
			if(api != null)
				api.setTerms(content);
			break;

		case "company":
			if(api != null)
				api.setCompany(content);
			break;
		case "updated":
			if(api != null)
				api.setUpdated(Timestamp.valueOf(content.replace("T"," ").replace("Z","")));
			break;

		case "openSearch:totalResults":
			this.setTotalResults(Integer.parseInt(content));
			break;
		case "openSearch:startIndex":
			this.setStartIndex(Integer.parseInt(content));
			break;
		case "openSearch:itemsPerPage":
			this.setItemsPerPage(Integer.parseInt(content));
			break;
		default:
			break;
		}
	}

	public boolean hasMoreApis(){
		return (itemsPerPage + startIndex < totalResults);
	}

	@Override
	public void characters(char[] ch, int start, int length) 
			throws SAXException {
		content = String.copyValueOf(ch, start, length).trim();
	}

}
