/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * @author BarinderSingh
 *
 */
public class MySQLDatabaseConnectionHandler {

	private static final String DB_CONNECTION_STRING = "jdbc:mysql://localhost/programmableweb_database";
	private static final String DB_USERNAME = "root";
	private static final String DB_PASSWORD = "";
	private static volatile Connection dbConnection;
	
	private MySQLDatabaseConnectionHandler(){}
	
	public static Connection getMySQLDBConnectionInstance() throws ClassNotFoundException, SQLException{
		if(dbConnection == null){
			synchronized(MySQLDatabaseConnectionHandler.class){
				if(dbConnection == null){
					Class.forName("com.mysql.jdbc.Driver");
					dbConnection = DriverManager.getConnection(DB_CONNECTION_STRING, DB_USERNAME, DB_PASSWORD);
					dbConnection.setAutoCommit(false);
				}
			}
		}
		return dbConnection;
	}
	
	public static void closeMySQLDBConnection() throws SQLException{
		if(!dbConnection.isClosed())
			dbConnection.close();
	}
}