/**
 * 
 */
package com.programmableweb.restwebservice.client;


import java.sql.Timestamp;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author BarinderSingh
 *
 */
public class PWMashupSAXHandler extends DefaultHandler{

	List<PWMashup> mashupList;
	PWMashup mashup;
	StringBuffer content;
	int totalResults;
	int startIndex;
	int itemsPerPage;
	boolean authorNameFlag;
	boolean isTag, isApi;

	public PWMashupSAXHandler(){
		mashupList = new LinkedList<PWMashup>();
		mashup = null;
		content = new StringBuffer();
		totalResults = 0;
		startIndex = 0;
		itemsPerPage = 0;
		authorNameFlag = false;
		isTag = false;
		isApi = false;
	}

	/**
	 * @param totalResults the totalResults to set
	 */
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * @param itemsPerPage the itemsPerPage to set
	 */
	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * @return the totalResults
	 */
	public int getTotalResults() {
		return totalResults;
	}

	/**
	 * @return the startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * @return the itemsPerPage
	 */
	public int getItemsPerPage() {
		return itemsPerPage;
	}

	@Override
	//Triggered when the start of tag is found.
	public void startElement(String uri, String localName, 
			String qName, Attributes attributes) 
					throws SAXException {

		content = new StringBuffer();
		switch(qName){
		//Create a new member object when the start tag is found
		case "entry":
			mashup = new PWMashup();
			break;
		case "author":
			authorNameFlag = true;
			break;
		case "tags":
			if(mashup != null){
				mashup.setTags(new LinkedHashSet<String>());
			}
			break;
		case "apis":
			if(mashup != null){
				mashup.setApis(new LinkedHashSet<String>());
			}
			break;
		case "tag":
			if(mashup != null)
				isTag = true;
			break;
		case "api":
			if(mashup != null)
				isApi = true;
			break;
		default:
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, 
			String qName) throws SAXException {
		switch(qName){
		//Add the employee to list once end tag is found
		case "entry":
			mashupList.add(mashup);  
			//content = "";
			break;
			//For all other end tags the employee has to be updated.
		case "id":
			if(mashup != null){
				mashup.setMashupId(content.toString());
				mashup.setMashupName(content.toString().replaceAll(".*/mashup/", ""));
			}
			//content = "";
			break;
		case "title":
			if(mashup != null) 
				mashup.setTitle(content.toString());
			//content = "";
			break;
		case "author":
			if(mashup != null)
				authorNameFlag = false;
			//content = "";
			break;
		case "name":
			if(mashup != null && authorNameFlag)
				mashup.setAuthorName(content.toString());
			//content = "";
			break;
		case "summary":
			if(mashup != null)
				mashup.setSummary(content.toString());
			//content = "";
			break;

		case "rating":
			if(mashup != null){
				if(content == null || content.toString().equals(""))
					content.append("0.0");
				mashup.setRating(Float.parseFloat(content.toString()));
			}
			//content = "";
			break;

		case "description":
			if(mashup != null)
				mashup.setDescription(content.toString());
		//	content = "";
			break;

		case "useCount":
			if(mashup != null){
				if(content == null || content.toString().equals(""))
					content.append("0");
				mashup.setUseCount(Integer.parseInt(content.toString()));
			}
			//content = "";
			break;

		case "dateModified":
			if(mashup != null)
				mashup.setDateModified(Timestamp.valueOf(content.toString().replace("T"," ").replace("Z","")));
			//content = "";
			break;

		case "numComments":
			if(mashup != null){
				if(content == null || content.toString().equals(""))
					content.append("0");
				mashup.setNumComments(Integer.parseInt(content.toString()));
			}
			//content = "";
			break;

			/*case "tag":
			if(mashup != null)
				mashup.getTags().add(content);
			break;*/

		case "apis":
			if(mashup != null)
				isApi = false;
			//content = new StringBuffer();
			break;

		case "tags":
			if(mashup != null)
				isTag = false;
			//content = new StringBuffer();
			break;


		case "url":
			if(mashup != null){
				if( content != null && !content.toString().equals("")) {
					if(isApi) {
						mashup.getApis().add(content.toString().replaceAll(".*/api/", ""));
					} else if (isTag){
						mashup.getTags().add(content.toString().replaceAll(".*/tag/", ""));
					}
				}
			}
			//content = new StringBuffer();
			break;

		case "updated":
			if(mashup != null){
				mashup.setUpdated(Timestamp.valueOf(content.toString().replace("T"," ").replace("Z","")));
			}
			//content = new StringBuffer();
			break;

		case "openSearch:totalResults":
			this.setTotalResults(Integer.parseInt(content.toString()));
			//content = new StringBuffer();
			break;
		case "openSearch:startIndex":
			this.setStartIndex(Integer.parseInt(content.toString()));
			//content = new StringBuffer();
			break;
		case "openSearch:itemsPerPage":
			this.setItemsPerPage(Integer.parseInt(content.toString()));
			//content = new StringBuffer();
			break;
		default:
			//content = new StringBuffer();
			break;
		}
	}

	public boolean hasMoreMashups(){
		return (itemsPerPage + startIndex < totalResults);
	}

	@Override
	public void characters(char[] ch, int start, int length) 
			throws SAXException {
		//content = String.copyValueOf(ch, start, length).trim();
		content.append(new String(ch, start, length).trim());
	}

}
