/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.sql.Timestamp;
import java.util.Set;

/**
 * @author BarinderSingh
 *
 */
public class PWMashup {


	private String mashupId;
	private String mashupName;
	private String authorName;
	private String summary;
	private float rating;
	private String description;
	private int useCount;
	private Timestamp dateModified;
	private int numComments;
	private Timestamp updated;
	private Set<String> tags;
	private Set<String> apis;
	private String title;
	
	
	public String getTagsForDatabase(){
		String tags = "";
		boolean flag = false;
		for(String tag: this.tags){
			tags += (tag + ",");
			flag = true;
		}
		if(flag == true)
			tags = tags.substring(0, tags.length()-1);
		
		return tags;
	}
	


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the mashupId
	 */
	public String getMashupId() {
		return mashupId;
	}

	/**
	 * @param mashupId the mashupId to set
	 */
	public void setMashupId(String mashupId) {
		this.mashupId = mashupId;
	}

	/**
	 * @return the mashupName
	 */
	public String getMashupName() {
		return mashupName;
	}

	/**
	 * @param mashupName the mashupName to set
	 */
	public void setMashupName(String mashupName) {
		this.mashupName = mashupName;
	}

	/**
	 * @return the authorName
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * @param authorName the authorName to set
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param summary the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @return the rating
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the useCount
	 */
	public int getUseCount() {
		return useCount;
	}

	/**
	 * @param useCount the useCount to set
	 */
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	/**
	 * @return the dateModified
	 */
	public Timestamp getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the numComments
	 */
	public int getNumComments() {
		return numComments;
	}

	/**
	 * @param numComments the numComments to set
	 */
	public void setNumComments(int numComments) {
		this.numComments = numComments;
	}

	/**
	 * @return the updated
	 */
	public Timestamp getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	/**
	 * @return the tags
	 */
	public Set<String> getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	/**
	 * @return the apis
	 */
	public Set<String> getApis() {
		return apis;
	}

	/**
	 * @param apis the apis to set
	 */
	public void setApis(Set<String> apis) {
		this.apis = apis;
	}

	
	public String getApisForDatabase(){
		String apis = "";
		boolean flag = false;
		for(String api: this.apis){
			apis += (api + ",");
			flag = true;
		}
		if(flag == true)
			apis = apis.substring(0, apis.length()-1);
		
		return apis;
	}

	
	@Override
	public String toString(){
		return "{userId = " + this.mashupId + "\n"
				+ "title = " + this.title + "\n"
				+ "authorName = " + this.authorName + "\n"
				+ "summary = " + this.summary + "\n"
				+ "dateModified = " + this.dateModified + "}";
	}
	
}
