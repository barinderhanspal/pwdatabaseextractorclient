/**
 * 
 */
package com.programmableweb.restwebservice.client;


import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author BarinderSingh
 *
 */
public class PWMemberSAXHandler extends DefaultHandler{

	List<PWMember> memberList;
	PWMember member;
	String content;
	int totalResults;
	int startIndex;
	int itemsPerPage;

	public PWMemberSAXHandler(){
		memberList = new LinkedList<PWMember>();
		member = null;
		content = null;
		totalResults = 0;
		startIndex = 0;
		itemsPerPage = 0;
	}

	/**
	 * @param totalResults the totalResults to set
	 */
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * @param itemsPerPage the itemsPerPage to set
	 */
	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * @return the totalResults
	 */
	public int getTotalResults() {
		return totalResults;
	}

	/**
	 * @return the startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * @return the itemsPerPage
	 */
	public int getItemsPerPage() {
		return itemsPerPage;
	}

	@Override
	//Triggered when the start of tag is found.
	public void startElement(String uri, String localName, 
			String qName, Attributes attributes) 
					throws SAXException {

		switch(qName){
		//Create a new member object when the start tag is found
		case "entry":
			member = new PWMember();
			break;
		default:
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, 
			String qName) throws SAXException {
		switch(qName){
		//Add the employee to list once end tag is found
		case "entry":
			memberList.add(member);       
			break;
			//For all other end tags the employee has to be updated.
		case "title":
			if(member != null)
				member.setUserId(content.trim());
			break;
		case "id":
			if(member != null)
				member.setProfileUrl(content);
			break;
		case "location":
			if(member != null)
				member.setLocation(content); 
			break;
		case "longitude":
			if(member != null)
				member.setLongitude(Float.parseFloat(content));
			break;
		case "latitude":
			if(member != null)
				member.setLatitude(Float.parseFloat(content));
			break;
		case "country":
			if(member != null)
				member.setCountry(content);
			break;
		case "updated":
			if(member != null)
				member.setRegistrationDate(Timestamp.valueOf(content.replace("T"," ").replace("Z","")));
			break;
		case "openSearch:totalResults":
			this.setTotalResults(Integer.parseInt(content));
			break;
		case "openSearch:startIndex":
			this.setStartIndex(Integer.parseInt(content));
			break;
		case "openSearch:itemsPerPage":
			this.setItemsPerPage(Integer.parseInt(content));
			break;
		default:
			break;
		}
	}
	
	public boolean hasMoreMembers(){
		return (itemsPerPage + startIndex < totalResults);
	}

	@Override
	public void characters(char[] ch, int start, int length) 
			throws SAXException {
		content = String.copyValueOf(ch, start, length).trim();
	}

}
