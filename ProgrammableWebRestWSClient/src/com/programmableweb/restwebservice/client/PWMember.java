/**
 * 
 */
package com.programmableweb.restwebservice.client;

import java.sql.Timestamp;

/**
 * @author BarinderSingh
 *
 */ 
public class PWMember {

	private String userId;
	private String profileUrl;
	private float longitude;
	private float latitude;
	private String location;
	private String country;
	private String mashups;
	private String favorites;
	private String friends;
	private Timestamp registrationDate;
	
	public PWMember() {}
	
	public PWMember(String userId, String profileUrl, float longitude, float latitude, 
						String location, String country, String mashups, String favorites, String friends, Timestamp registrationDate) {
		
		this.userId = userId;
		this.profileUrl = profileUrl;
		this.longitude = longitude;
		this.latitude = latitude;
		this.location = location;
		this.country = country;
		this.mashups = mashups;
		this.favorites = favorites;
		this.friends = friends;
		this.registrationDate = registrationDate;
		
	}   
	
	/**
	 * @return the mashups
	 */
	public String getMashups() {
		return mashups;
	}

	/**
	 * @param mashups the mashups to set
	 */
	public void setMashups(String mashups) {
		this.mashups = mashups;
	}

	/**
	 * @return the favorites
	 */
	public String getFavorites() {
		return favorites;
	}

	/**
	 * @param favorites the favorites to set
	 */
	public void setFavorites(String favorites) {
		this.favorites = favorites;
	}

	/**
	 * @return the friends
	 */
	public String getFriends() {
		return friends;
	}

	/**
	 * @param friends the friends to set
	 */
	public void setFriends(String friends) {
		this.friends = friends;
	}

	@Override
	public String toString(){
		return "{userId = " + this.userId + "\n"
				+ "profileUrl = " + this.profileUrl + "\n"
				+ "longitude = " + this.longitude + "\n"
				+ "latitude = " + this.latitude + "\n"
				+ "location = " + this.location + "\n"
				+ "country = " + this.country + "\n"
				+ "registrationDate = " + this.registrationDate + "}";
	}
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the profileUrl
	 */
	public String getProfileUrl() {
		return profileUrl;
	}
	/**
	 * @param profileUrl the profileUrl to set
	 */
	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the registrationDate
	 */
	public Timestamp getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
}
